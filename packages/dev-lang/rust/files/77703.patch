From 593535ec4ffa8541bd002a3d9b03e1a4e88631a2 Mon Sep 17 00:00:00 2001
From: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
Date: Thu, 8 Oct 2020 15:05:31 +0200
Subject: [PATCH] add system-llvm-libunwind config option

allows using the system-wide llvm-libunwind as the unwinder

Signed-off-by: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
---
 config.toml.example        | 3 ++-
 library/std/Cargo.toml     | 1 +
 library/test/Cargo.toml    | 1 +
 library/unwind/Cargo.toml  | 1 +
 library/unwind/build.rs    | 4 +++-
 src/bootstrap/config.rs    | 6 +++---
 src/bootstrap/configure.py | 2 +-
 src/bootstrap/lib.rs       | 6 ++++--
 8 files changed, 16 insertions(+), 8 deletions(-)

diff --git a/config.toml.example b/config.toml.example
index 36587cc0784..0ebd102538b 100644
--- a/config.toml.example
+++ b/config.toml.example
@@ -469,7 +469,8 @@
 #test-compare-mode = false
 
 # Use LLVM libunwind as the implementation for Rust's unwinder.
-#llvm-libunwind = false
+# Accepted values are 'in-tree' or 'system'.
+#llvm-libunwind = 'no'
 
 # Enable Windows Control Flow Guard checks in the standard library.
 # This only applies from stage 1 onwards, and only for Windows targets.
diff --git a/library/std/Cargo.toml b/library/std/Cargo.toml
index ef0ef415b4c..2e39e775db9 100644
--- a/library/std/Cargo.toml
+++ b/library/std/Cargo.toml
@@ -60,6 +60,7 @@ panic-unwind = ["panic_unwind"]
 profiler = ["profiler_builtins"]
 compiler-builtins-c = ["alloc/compiler-builtins-c"]
 llvm-libunwind = ["unwind/llvm-libunwind"]
+system-llvm-libunwind = ["unwind/system-llvm-libunwind"]
 
 # Make panics and failed asserts immediately abort without formatting any message
 panic_immediate_abort = ["core/panic_immediate_abort"]
diff --git a/library/test/Cargo.toml b/library/test/Cargo.toml
index 7b76dc83aa2..e304c8e6d47 100644
--- a/library/test/Cargo.toml
+++ b/library/test/Cargo.toml
@@ -26,6 +26,7 @@ default = ["std_detect_file_io", "std_detect_dlsym_getauxval", "panic-unwind"]
 backtrace = ["std/backtrace"]
 compiler-builtins-c = ["std/compiler-builtins-c"]
 llvm-libunwind = ["std/llvm-libunwind"]
+system-llvm-libunwind = ["std/system-llvm-libunwind"]
 panic-unwind = ["std/panic_unwind"]
 panic_immediate_abort = ["std/panic_immediate_abort"]
 profiler = ["std/profiler"]
diff --git a/library/unwind/Cargo.toml b/library/unwind/Cargo.toml
index 8e2db217c31..c1a7f19b183 100644
--- a/library/unwind/Cargo.toml
+++ b/library/unwind/Cargo.toml
@@ -23,3 +23,4 @@ cc = { version = "1.0.1" }
 
 [features]
 llvm-libunwind = []
+system-llvm-libunwind = []
diff --git a/library/unwind/build.rs b/library/unwind/build.rs
index ab09a6e324d..050ca32ce6e 100644
--- a/library/unwind/build.rs
+++ b/library/unwind/build.rs
@@ -4,7 +4,9 @@ fn main() {
     println!("cargo:rerun-if-changed=build.rs");
     let target = env::var("TARGET").expect("TARGET was not set");
 
-    if cfg!(feature = "llvm-libunwind")
+    if cfg!(feature = "system-llvm-libunwind") {
+        println!("cargo:rustc-link-lib=unwind");
+    } else if cfg!(feature = "llvm-libunwind")
         && ((target.contains("linux") && !target.contains("musl")) || target.contains("fuchsia"))
     {
         // Build the unwinding from libunwind C/C++ source code.
diff --git a/src/bootstrap/config.rs b/src/bootstrap/config.rs
index 8b8b01b1153..4a38c6e3483 100644
--- a/src/bootstrap/config.rs
+++ b/src/bootstrap/config.rs
@@ -51,7 +51,7 @@ pub struct Config {
     pub rustc_error_format: Option<String>,
     pub json_output: bool,
     pub test_compare_mode: bool,
-    pub llvm_libunwind: bool,
+    pub llvm_libunwind: Option<String>,
 
     pub skip_only_host_steps: bool,
 
@@ -409,7 +409,7 @@ struct Rust {
     remap_debuginfo: Option<bool>,
     jemalloc: Option<bool>,
     test_compare_mode: Option<bool>,
-    llvm_libunwind: Option<bool>,
+    llvm_libunwind: Option<String>,
     control_flow_guard: Option<bool>,
     new_symbol_mangling: Option<bool>,
 }
@@ -645,7 +645,7 @@ impl Config {
             set(&mut config.rust_rpath, rust.rpath);
             set(&mut config.jemalloc, rust.jemalloc);
             set(&mut config.test_compare_mode, rust.test_compare_mode);
-            set(&mut config.llvm_libunwind, rust.llvm_libunwind);
+            config.llvm_libunwind = rust.llvm_libunwind.clone();
             set(&mut config.backtrace, rust.backtrace);
             set(&mut config.channel, rust.channel.clone());
             set(&mut config.rust_dist_src, rust.dist_src);
diff --git a/src/bootstrap/configure.py b/src/bootstrap/configure.py
index 47673ce1e87..e156952d56f 100755
--- a/src/bootstrap/configure.py
+++ b/src/bootstrap/configure.py
@@ -65,7 +65,7 @@ v("llvm-cflags", "llvm.cflags", "build LLVM with these extra compiler flags")
 v("llvm-cxxflags", "llvm.cxxflags", "build LLVM with these extra compiler flags")
 v("llvm-ldflags", "llvm.ldflags", "build LLVM with these extra linker flags")
 
-o("llvm-libunwind", "rust.llvm-libunwind", "use LLVM libunwind")
+v("llvm-libunwind", "rust.llvm-libunwind", "use LLVM libunwind")
 
 # Optimization and debugging options. These may be overridden by the release
 # channel, etc.
diff --git a/src/bootstrap/lib.rs b/src/bootstrap/lib.rs
index a42ee11bd6f..e8670f91760 100644
--- a/src/bootstrap/lib.rs
+++ b/src/bootstrap/lib.rs
@@ -525,8 +525,10 @@ impl Build {
     fn std_features(&self) -> String {
         let mut features = "panic-unwind".to_string();
 
-        if self.config.llvm_libunwind {
-            features.push_str(" llvm-libunwind");
+        match self.config.llvm_libunwind.as_deref() {
+            Some("in-tree") => features.push_str(" llvm-libunwind"),
+            Some("system") => features.push_str(" system-llvm-libunwind"),
+            _ => {}
         }
         if self.config.backtrace {
             features.push_str(" backtrace");
-- 
2.29.0.rc0

