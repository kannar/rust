# Copyright 2020 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo systemd-service [ systemd_files=[ units/swap-create@.service ] ]

SUMMARY="Systemd unit generator for zram swap devices."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"

DEPENDENCIES="
"

generator_dir() {
    "${PKG_CONFIG}" --variable=systemdsystemgeneratordir systemd
}

src_compile() {
    cargo_src_compile
    # FIXME: make man, requires ronn
    edo rm -r man/
    edo sed -e "s,@SYSTEMD_SYSTEM_GENERATOR_DIR@,$(generator_dir)," \
        < units/swap-create@.service.in \
        > units/swap-create@.service

}

src_install() {
    cargo_src_install
    edo mkdir -p "${IMAGE}"/$(generator_dir)
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/${PN} "${IMAGE}"/$(generator_dir)
    dodoc zram-generator.conf.example
    edo find "${IMAGE}" -type d -empty -delete
    install_systemd_files
}
